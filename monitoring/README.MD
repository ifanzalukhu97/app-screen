## Frontend Url
- [Release](https://simplidots-subscription.azurewebsites.net/)
- [Dev](https://simplidots-subscription-frontend-dev.azurewebsites.net/)

## Backend API
- [Dev](https://simplidots-subscription-api-dev.azurewebsites.net)
- [Prod](https://simplidots-subscription-api-prod.azurewebsites.net)

## Database Name
- **Dev** : simplidots_subscription_db
- **Production**: simplidots_subscription_db_release